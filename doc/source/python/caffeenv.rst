Caffe installation in Ubuntu 16.04
**********************************

Prerequisite
============

For GPU support, we need to install CUDA as well. 

*  Ubuntu library installation

.. code-block:: shell

    sudo apt-get install -y --force-yes build-essential autoconf libtool libopenblas-dev libgflags-dev libgoogle-glog-dev libopencv-dev protobuf-compiler libleveldb-dev liblmdb-dev libhdf5-dev libsnappy-dev libboost-all-dev libssl-dev
     
    sudo apt-get install lsb-core
    sudo apt-get install libyaml-cpp-dev
     
     
    Above should work fine. If something went wrong then use below:
        sudo apt-get install libhdf5*
        sudo apt-get install libglog*
        sudo apt install libgoogle-glog-dev
        sudo apt install libboost*
        sudo apt-get install libleveldb*
        sudo apt-get install liblmdb*
        sudo apt-get install libopenblas*


* Cuda installation 

Download and install cuda (cuda-9.0 can be downloaded from https://developer.nvidia.com/cuda-90-download-archive?target_os=Linux&target_arch=x86_64&target_distro=Ubuntu&target_version=1604&target_type=deblocal)

Extract cuda-repo-ubuntu1604-9-0-local_9.0.176-1_amd64.deb and run the below commands

.. code-block:: shell

    $ sudo dpkg -i cuda-repo-ubuntu1604-9-0-local_9.0.176-1_amd64.deb
    (press tab after cuda-repo and then look for .pub file)
    $ sudo apt-key add /var/cuda-repo-9-0-local/7fa2af80.pub
    $ sudo apt-get update
    $ sudo apt-get install cuda


* Install cuDNN-7.0.5 : Download cuDNN v7.0.5 (Dec 5, 2017), for CUDA 9.0, cuDNN v7.0.5 Library for Linux, which is available at below link, 

https://developer.nvidia.com/rdp/cudnn-archive

.. code-block:: shell

    Extract cudnn-9.0-linux-x64-v7.tgz and run below commands
    (do not go inside the extracted folder 'cuda')
    sudo cp cuda/include/cudnn.h /usr/local/cuda/include
    sudo cp cuda/lib64/libcudnn* /usr/local/cuda/lib64
    sudo chmod a+r /usr/local/cuda/include/cudnn.h /usr/local/cuda/lib64/libcudnn*
    cd /usr/local/cuda/lib64/
    sudo chmod +r libcudnn.so.7.0.5
    sudo ln -sf libcudnn.so.7.0.5 libcudnn.so.7
    sudo ln -sf libcudnn.so.7 libcudnn.so
    sudo ldconfig

*  Install Anaconda (try to install the default Ubuntu Python)

Install packages
================


.. code-block:: shell

    $ sudo apt-get update
    $ sudo apt-get upgrade

    $ sudo apt-get install build-essential cmake git unzip pkg-config libjpeg8-dev libtiff5-dev libjasper-dev libpng12-dev libgtk2.0-dev libgtk-3-dev libavcodec-dev libavformat-dev libswscale-dev libv4l-dev libatlas-base-dev gfortran

    $ sudo apt-get install libprotobuf-dev libleveldb-dev libsnappy-dev libhdf5-serial-dev protobuf-compiler libboost-all-dev

    $ sudo apt-get install libgflags-dev libgoogle-glog-dev liblmdb-dev

    $ sudo apt-get install python3-dev python2.7-dev

    (below is required to remove the gcc related issues)
    $ sudo apt-get install libstdc++6
    $ sudo add-apt-repository ppa:ubuntu-toolchain-r/test & sudo apt-get update & sudo apt-get upgrade && sudo apt-get dist-upgrade



Create virtualenv
=================

.. note:: 

    We will need virtualenv-package for this procces of installation; i.e. 'conda create' will not work properly. Once caffe is installed successfully, then we can use the below commands as well to create environment. 

    Also, we need to install 'libgcc', if we are using the conda environment to run the caffe as shown below, 

    .. code-block:: shell
    
        $ conda create -n caffe python=3.6
        $ pip install numpy
        $ conda install libgcc



Install virtualenv
------------------


* Install virtualenv and virtualenvwrapper
  
.. code-block:: shell

    $ pip install virtualenv virtualenvwrapper



* If pip is not available, then use the below command,   

.. code-block:: shell
    
    $ wget https://bootstrap.pypa.io/get-pip.py
    $ sudo python get-pip.py
    $ pip install virtualenv virtualenvwrapper
    $ sudo rm -rf get-pip.py ~/.cache/pip    


Modify .bashrc
--------------

* Add below line at the end of .bashrc file if virtual environment is installed in the Anaconda environment


.. code-block:: shell

    # replace /home/meherp/anaconda2 with correct installation location 

    # virtualenv and virtualenvwrapper
    export WORKON_HOME=$HOME/.virtualenvs
    export VIRTUALENVWRAPPER_PYTHON=/home/meherp/anaconda2/bin/python
    source /home/meherp/anaconda2/bin/virtualenvwrapper.sh

* Add below at the end of .bashrc file if virtualenv environment is created outside the Anaconda environment. 


.. code-block:: shell

    # replace python3 with correct version i.e. python3 or python2

    export WORKON_HOME=$HOME/.virtualenvs
    source /usr/local/bin/virtualenvwrapper.sh
    export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3


Create virtualenv
-----------------

Test the installation by creating a virtualenv and installing numpy to it

$ source ~/.bashrc
$ mkvirtualenv caffev -p python3
$ pip install numpy


Install OpenCV
===============

* activate the environment, 

.. code-block:: shell

    $ workon caffev


* If virtualenv is created using conda, then we need to install below package in every virtualenv, 

.. code-block:: shell

    $ conda install libgcc


* Install OpenCV-3 using pip command, 

.. code-block:: shell

    pip install opencv-python==3.4.5.20


* Use below if above does not work at the end, while installing the caffe

.. code-block:: shell
    
    $ wget -O opencv.zip https://github.com/opencv/opencv/archive/3.4.4.zip
    $ wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/3.4.4.zip
    $ unzip opencv.zip
    $ unzip opencv_contrib.zip
    $ mv opencv-3.4.4 opencv
    $ mv opencv_contrib-3.4.4 opencv_contrib
    $ cd opencv
    $ mkdir build
    $ cd build

    (give correct location for opencv_contrib)
    $ cmake -D CMAKE_BUILD_TYPE=RELEASE \
        -D CMAKE_INSTALL_PREFIX=/usr/local \
        -D WITH_CUDA=OFF \
        -D INSTALL_PYTHON_EXAMPLES=ON \
        -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules \
        -D OPENCV_ENABLE_NONFREE=ON \
        -D BUILD_EXAMPLES=ON ..
    $ make -j4
    $ sudo make install

    (if sudo failed, use witout sudo and create directory as below)
        make install
        (if directory creatinon failed then)
        sudo mkdir -p /usr/local/share/OpenCV/licenses
        make install 
        (it will fail again)
        sudo make install 

    $ sudo ldconfig
    
        
    (add cv to virtualenvs)
    cd ~/.virtualenvs/caffev/lib/python3.5/site-packages/
    ln -s /usr/local/python/cv2 cv2



Install Caffe
=============

* Activate the environment, 

.. code-block:: shell

    $ workon caffev


* If virtualenv is created using conda, then we need to install below package in every virtualenv, 


.. code-block:: shell

    $ conda install libgcc



* select a location to build the caffe, we will use this location to build the caffe model in future for different examples 

.. code-block:: shell

    $ cd <location to download and build caffe>
    $ git clone https://github.com/BVLC/caffe.git
    $ cd caffe/python

* Modify requirements.txt as below i.e. remove the version number from the end of python-dateutil, 

.. code-block:: text
    :linenos: 
    :emphasize-lines: 12

    Cython>=0.19.2
    numpy>=1.7.1
    scipy>=0.13.2
    scikit-image>=0.9.3
    matplotlib>=1.3.1
    ipython>=3.0.0
    h5py>=2.2.0
    leveldb>=0.191
    networkx>=1.8.1
    nose>=1.3.0
    pandas>=0.12.0
    python-dateutil
    protobuf>=2.5.0
    python-gflags>=2.0
    pyyaml>=3.10
    Pillow>=2.3.0
    six>=1.1.0


* Now install packages

.. code-block:: shell

    $ for req in $(cat requirements.txt); do pip install $req; done


* Modify following three lines in the caffe/CMakeLists.txt file, 

.. code-block:: text

    (go to caffe folder)
    cd ..

    and modify below 3 lines

    # (for GPU, put off)
    caffe_option(CPU_ONLY  "Build Caffe without CUDA support" OFF) # TODO: rename to USE_CUDA

    # (python 2 or 3: 3 in below )
    set(python_version "3" CACHE STRING "Specify which Python version to use")

    # (turn on opencv as it is installed)
    caffe_option(USE_OPENCV "Build with OpenCV support" ON)


* Build caffe

.. code-block:: shell

    # create 'build' folder inside caffe

    $ mkdir build
    $ cd build
    $ cmake ..
    $ make all -j4
    $ make runtest


* add below line in .bashrc i.e. location of caffe/python as below, 

.. code-block:: text

  export PYTHONPATH=/home/meherp/caffe/python:$PYTHONPATH

* Install necessary packages

.. code-block:: shell

    $ pip install imutils pillow progressbar2 sh json-minify scikit-learn scikit-image matplotlib


* Verify installation 

.. code-block:: shell

    $ python

    >>> import caffe
