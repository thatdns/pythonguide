.. Advance Python Tutorial documentation master file, created by
   sphinx-quickstart on Wed Oct 18 07:33:44 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Python Tutorials
================

.. toctree::
   :maxdepth: 3
   :numbered:
   :caption: Contents:

   python/review
   python/package
   python/debug
   python/print
   python/csv
   python/function
   python/datatypes
   python/exception
   python/datamine
   python/oops
   python/property
   python/decorator
   python/moreex
   python/unix
   python/caffeenv

.. only:: html

   Index
   =====

   * :ref:`genindex`

   .. * :ref:`modindex`
   .. * :ref:`search`
