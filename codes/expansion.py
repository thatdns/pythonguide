# expansion.py

# find the number of days when radius = 10 cm due to heat-expansion

import sys

if len(sys.argv) != 3: # display error message for missing arguments
    raise SystemExit("usage : ring.py \"metal\" radius")

# sys.argv[0] is the file name 
metal = sys.argv[1] 

# input is read as string therefore it is converted into float
radius = float(sys.argv[2])

# list of expansion rate for different metal
rate = [0.4, 0.08, 0.05] # [Copper, Gold, Iron]

day = 0

out_file = open("expansion.txt", "w") # open file in write mode

## use any of the below
## below is not in good format
# print("{0}, {1}, {2}".format("day", "expansion", "radius"))

## "> right aligned" "< left aligned"
## 5s : string with width 5
print("{:>5s} {:>10s} {:>7s}".format("day", "expansion", "radius"), 
                file = out_file)

## old style
# print("%5s %10s %7s" % ("day", "expansion", "radius"))

while radius < 10:
    # multiply by correct expansion rate
    if metal == "Copper":
        expansion = radius * rate[0]
    elif metal == "Gold":
        expansion = radius * rate[1] 
    elif metal == "Iron":
        expansion = radius * rate[2]
    else: 
        print("Enter the correct metal")
        break

    # new radius
    radius += expansion
    day += 1 # increment the number of days by one
    
    ## print the data
    ## 5d : 5 digits 
    ## 7.2f : 7 digits with 2 decimal points
    print("{:>5d} {:>10.5f} {:>7.2f}".format(day, expansion, radius), 
                file = out_file)

# print the number of days
# print("Number of days =", day)



