# cat.py

import argparse

parser = argparse.ArgumentParser(description="First argparse code")

# read arguments 
parser.add_argument(
        nargs='+', # read multiple values in a list
        dest='files', # name of the attribute returned by argparse
        help='unix "cat" operation')

# line number
parser.add_argument(
        '-n', '--numbers', # default 'dest=numbers'
        action='store_true', help='print line numbers')

args = parser.parse_args() # save arguments in args
# print(f"args: {args.files}") # print argument read by 'dest=files'
# print(f"first element: {args.files[0]}") # print first element of arg.files


# open and read each files
line_no = 0
for file_name in args.files:
    with open(file_name, 'r') as w:
        if args.numbers:  # print data with line numbers
            for data in w.readlines(): # start from 1
                line_no += 1
                print("{0:5d}\t {1}".format(line_no, data), end='') 
        else: # print data without line numbers
            data = w.read()
            print(data)



